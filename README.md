# xxmy-format-tools

[![GitHub](https://img.shields.io/npm/l/xxmy-format-tools)](https://gitee.com/txlong/xxmy-format-tools/blob/master/LICENSE)
[![npm](https://img.shields.io/npm/v/xxmy-format-tools?color=crimson&logo=npm)](https://www.npmjs.com/package/xxmy-format-tools)
[![deno](https://img.shields.io/npm/v/xxmy-format-tools?color=blue&label=demo&logo=wechat)](https://developers.weixin.qq.com/s/DybQgvmV7js3)

原生微信小程序 wxml 中实用格式化日期工具，具体可以导入[demo](https://developers.weixin.qq.com/s/DybQgvmV7js3)查看源码。

如果需要标准格式化，推荐使用[momentjs](http://momentjs.cn/)

## 安装

1. `npm install xxmy-format-tools`
2. 微信开发者工具编译 npm 包

## wxs

### format.wxs

_展示格式化：formatTime_

|     | 令牌      | 输出   |
| --: | :-------- | :----- |
|  年 | yyyy YYYY | `2021` |
|     | yy YY y Y | `21`   |
|  月 | MM        | `01`   |
|     | M         | `1`    |
|  日 | DD dd     | `03`   |
|     | D d       | `3`    |
|  时 | HH hh     | `09`   |
|     | H h       | `9`    |
|  分 | mm        | `08`   |
|     | m         | `08`   |
|  秒 | ss        | `04`   |
|     | s         | `4`    |

##### 使用

```html
<wxs src="../miniprogram_npm/xxmy-format-tools/format.wxs" module="format" />

<!-- 21-1-4 18:27:35 -->
<view class="date">{{format.formatTime(1609756055278,'y-M-D h:m:s')}}</view>
<!-- 2021-01-04 18:27:35 -->
<view class="date"
  >{{format.formatTime(1609756055278,'YYYY-MM-dd HH:mm:ss')}}</view
>
```

##### 追加脱敏工具

```html
<wxs module="wxs">
  module.exports.blurPhone = function (phone) {
    if (!phone) return ''
    return phone.replace(getRegExp('(\d{3})\d*(\d{4})'), '$1****$2');
  };
  module.exports.blurIdcard = function (idcard) {
    if (!idcard) return ''
    return idcard.replace(getRegExp('(\d{3})\d*(\d{4})'), '$1***********$2');
  };
  module.exports.getAge = function (idcard) {
    if (!idcard || idcard.length < 10) return ''
    return getDate().getFullYear() - idcard.substring(6, 10)
  };
</wxs>
```

---

如果你还有其他问题，欢迎关注公众号 「晓晓夢龑」( xiaoxiaomengyan3 )

![xiaoxiaomengyan3](https://gitee.com/txlong/static/raw/master/img/qrcode/qrcode_for_gh_309272258bf6_258.jpg)